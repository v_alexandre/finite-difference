#include <stdlib.h>
#include "Multimatrice.h"

double *Multimatrice(double **mat1, double *mat2, double *U, int *taille){
  int n = *taille;
  for(int i = 0; i < n; i++)
    {
	for(int k = 0; k < n; k++)
	{
	   	U[i] += mat1[i][k] * mat2[k];
	}	
    }
  return U;
}
