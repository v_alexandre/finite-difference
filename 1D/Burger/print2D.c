#include <stdlib.h>
#include <stdio.h>
#include "print2D.h"
void arr_print (double** pp, size_t x, size_t y)
{
  for(size_t i=0; i<x; i++)
  {
    for(size_t j=0; j<y; j++)
    {
      printf("%g ", pp[i][j]);
    }
    printf("\n");
  }
}
