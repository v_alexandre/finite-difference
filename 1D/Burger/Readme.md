```math
\begin{equation}
    \underbrace{\frac{\partial U}{\partial t}}_\text{dérivée temporelle}+\quad U\underbrace{\frac{\partial U}{\partial x}}_\text{terme convectif}=\frac{1}{Re}\underbrace{\frac{\partial^2 U}{\partial^2 x}}_\text{terme diffusif}
    \label{burger}
\end{equation}
```

```math
\begin{equation}
     \frac{U^{n+1}_{j}-U^n_{j}}{dt}=- U^n_j \frac{U^n_{j}-U^n_{j-1}}{dx}+\frac{1}{Re}\frac{U^{n+1}_{j+1}+U^{n+1}_{j-1}-2U^{n+1}_{j}}{dx^2}  \quad avec \ U\geqslant0 \nonumber\\
     \frac{U^{n+1}_{j}-U^n_{j}}{dt}=- U^n_j  \frac{U^n_{j+1}-U^n_{j}}{dx} +\frac{1}{Re}\frac{U^{n+1}_{j+1}+U^{n+1}_{j-1}-2U^{n+1}_{j}}{dx^2}\quad avec \ U<0 \nonumber
\end{equation}
```
```math
\begin{equation}
     U^{n+1}_{j-1}(\frac{-dt}{Redx^2})+U^{n+1}_{j}(1+\frac{2dt}{Redx^2})+U^{n+1}_{j+1}(\frac{-dt}{Redx^2})= U^n_j(1- \frac{dt}{dx}(U^n_{j}-U^n_{j-1})) \quad avec \ U\geqslant0 \nonumber\\
     U^{n+1}_{j-1}(\frac{-dt}{Redx^2})+U^{n+1}_{j}(1+\frac{2dt}{Redx^2})+U^{n+1}_{j+1}(\frac{-dt}{Redx^2})= U^n_j(1- \frac{dt}{dx}(U^n_{j+1}-U^n_{j}))   \quad avec \ U<0 \nonumber
\end{equation}
```

```math
\begin{equation}

\begin{pmatrix}
        
     1 & 0 & 0     & \cdots & 0 \\ 
    \frac{-dt}{Redx^2} & 1+\frac{2dt}{Redx^2} & \frac{-dt}{Redx^2}  & \cdots & 0 \\ 
    0 & \ddots & \ddots & \ddots & 0 \\
    \vdots & \ddots & \ddots & \ddots & \vdots \\
    0     &\cdots & \cdots & \frac{-dt}{Redx^2} & 1+\frac{2dt}{Redx^2} \\
    0 &\cdots & \cdots & \cdots & 1
\end{pmatrix}
\cdot
\begin{pmatrix}
    U_1^{n+1} \\
    U_2^{n+1}\\
    \vdots \\ 
    \vdots \\
    U_{N-1}^{n+1}\\
   U_N^{n+1}\\
\end{pmatrix}  
=
\begin{pmatrix}
    u_a(t_{n+1},x_{min}) \\
     \frac{\Delta t}{\Delta x} U_j^n(U_{j-1}^n-U_j^n) + U^n_j \quad avec \ U\geqslant0 \\
    \vdots \\ 
    \vdots \\
    \frac{\Delta t}{\Delta x} U_j^n(U_{j}^n-U_{j+1}^n) + U^n_j \quad avec \ U<0 \\
  u_a(t_{n+1},x_{max})\\
\end{pmatrix} 
\nonumber
\end{equation}
```
