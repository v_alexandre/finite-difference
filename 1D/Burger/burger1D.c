#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "Inverseur.h"
#include "Multimatrice.h"
#include "alloc2D.h"
#include "free2D.h"


struct Parametre
{
  double Longueur;
  int Nbrepoints;
  int tend;
  double Re;
};

int main(void)
{
  struct Parametre p = {4.,81,2,1.};

  double dx = p.Longueur/(p.Nbrepoints-1);
  double dt = dx/2.;
  double a = -dt/(p.Re*pow(dx,2));
  double b = 1+2*dt/(p.Re*pow(dx,2));
  double c = -dt/(p.Re*pow(dx,2));
  /**
     dynamic allocation table
  */
  double*x=NULL,*B=NULL, *u=NULL;	
  x= calloc(p.Nbrepoints,sizeof(double));
  u= calloc(p.Nbrepoints,sizeof(double));
  B= calloc(p.Nbrepoints, sizeof(double));

  /**
     initial exponential elevation
  */

  for(int i=0; i<p.Nbrepoints-1;i++)
      x[i+1] = x[i] + dx;

  for(int i =0; i<p.Nbrepoints-1;i++)
    u[i]=1.*exp(-(pow(x[i]-1.,2)/0.1));
  /** 
      Velocity Boundaries Conditions 
  */
  u[0]=0.;
  u[p.Nbrepoints-1]=0.;

  /** 
      Matrice B (table 1D)
  */
  for (int i=1;i<p.Nbrepoints-1;i++)
    B[i] = (u[i]*dt/dx)*(u[i-1]-u[i])+u[i];
  B[0] = u[0];
  B[p.Nbrepoints-1] = u[p.Nbrepoints-1];
/**
   Matrice inverse
*/
  double ** Ainv;
  Ainv = arr_alloc(p.Nbrepoints, p.Nbrepoints);
  Ainv = Matrice(&p.Nbrepoints, &a, &b, &c);

/**
   begin the time loop
*/
  FILE *fic = fopen("data.txt", "w");
  fprintf(fic, "%s %s %s\n", "#x", "#u", "#t");

  double t=0.;
  while(t<=p.tend){
    for(int i=0;i<p.Nbrepoints;i++)
      fprintf(fic, "%g %g %g\n", x[i], u[i], t);
    fflush(fic);
    u=Multimatrice(Ainv, B, u, &p.Nbrepoints);
    for (int i=1;i<p.Nbrepoints-1;i++)
      B[i] = (u[i]*dt/dx)*(u[i-1]-u[i])+u[i];
    B[0] = u[0] = 0.;
    B[p.Nbrepoints-1] = u[p.Nbrepoints-1] = 0.;
    fprintf(fic,"\n\n");
    t+=dt;
  }
	
  free(x);
  free(u);
  free(B);
  fclose(fic);
  arr_free(Ainv, p.Nbrepoints, p.Nbrepoints);
  return 0;
}
