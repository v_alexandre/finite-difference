#include <stdio.h>
#include <stdlib.h>
#include "Inverseur.h"
#include "Inv.h"
#include "alloc2D.h"
#include "print2D.h"
 
double **Matrice(int *taille, double *un, double *deux, double *trois){

/* Declaration variable qui contient la valeur de la variable pointee **/

	int n = *taille;
	double a = *un;
	double b = *deux;
	double c = *trois;

/* Initialisation table 2D **/

	double **mat = arr_alloc(n,n);
	
/* Boundaries Conditions **/
	
	mat[0][0]=1.;
	mat[n-1][n-1]=1.;

/* Table coefficients **/

	for (int i=1; i<n-1; i++){
		mat[i][i-1]=a;
		mat[i][i]=b;
		mat[i][i+1]=c;
	}

 	//arr_print(mat,n,n);
   mat = InvdiagMat(n, mat);
   return mat;
}

#if 0
	
	printf("Enter elements of matrix row wise:\n");
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
           scanf("%le", &mat[i][j]);
	
	printf("\nGiven matrix is:");
	for(int i = 0; i < n; i++){
		printf("\n");
		
		for(int j = 0; j < n; j++)
			printf("%g\t", mat[i][j]);
	}
#endif

/* Determinant calculation **/
#if 0
	double determinant = 0.;
	for(int i = 0; i < n; i++)
		determinant += (mat[0][i] * (mat[1][(i+1)%n] * mat[2][(i+2)%n] - mat[1][(i+2)%n] * mat[2][(i+1)%n]));
	if(determinant == 0)
		printf("%s","Attention determinant egale a 0");
	
	printf("\n\ndeterminant: %f\n", determinant);
	
	//printf("\nInverse of matrix is: \n");

/* Inverse matrice calculation **/
	double **Inverse = arr_alloc(n,n);
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++)
			Inverse[i][j]=((mat[(j+1)%n][(i+1)%n] * mat[(j+2)%n][(i+2)%n]) - (mat[(j+1)%n][(i+2)%n] * mat[(j+2)%n][(i+1)%n]))/ determinant;	
	}
#endif
  // double **Inverse = arr_alloc(n,n);
