Hello,

This code simulate 1D advection using finite difference. 

```math
\frac{\partial{U}}{\partial{t}} + U \frac{\partial{U}}{\partial{x}}=0
```
First order derivative in time: 
```math
\frac{\partial{U}}{\partial{t}} = \frac{U^{t+1}_{i} - U^{t}_{i}}{\Delta t} 
```
Upwind first order :
```math
\frac{\partial{U}}{\partial{x}} = \frac{U_{i}^{t} - U_{i-1}^{t}}{\Delta x}
```

which gives using euler explicite method :

```math
U^{t+1}_{i} = U^{t}_{i} - \frac{U^{t}_{i}\Delta t}{\Delta x}(U^{t}_{i}-U^{t}_{i-1})
```

How to compile : gcc 1Dadvection.c -o exe -lm

How to run : ./exe


![Gnuplot ouput](1D/Advection/figure.png)

![Gnuplot output pdf](1D/Advection/figure.pdf)



