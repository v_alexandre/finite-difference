set terminal pngcairo size 1200,800
set output 'figure.png'
#set terminal pdfcairo enhanced color notransparent
#set output 'figure.pdf'
set tics font ",15"
set key font ",15"

set title "Advection 1D Upwind 01" font ",15"
set xlabel "x" font ",15" offset 0, -0.5
set ylabel "Wave" font ",15"  offset -0.5, 0

plot 'data.txt' u 1:($3==0.?$2:1/0) w l lw 2 t 't=0.',\
     '' u 1:($3==0.5?$2:1/0) w l lw 2 t 't=0.5',\
     '' u 1:($3==1.?$2:1/0) w l lw 2 t 't=1.'
