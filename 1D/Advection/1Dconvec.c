#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main()
{
	int nx=81;
	double dx=2./(nx-1);
	int tend=2;
	double dt=dx/5;
	double*x=NULL,*U=NULL, *u=NULL;	
/**
 dynamic allocation 
*/
	x= calloc(nx,sizeof(double));
	U= calloc(nx,sizeof(double));
	u= calloc(nx,sizeof(double));
  
/**
 Boundary condition
*/
	u[0] = 0.;
	u[nx-1]=0.;
	
/**
initial elevation: a "bump", position 
*/
	static double t=0.;
	for(int i=0; i<nx;i++)
	{
		x[i+1] = x[i] + dx;
		u[i] = 1.;
	}
	for(int i =10; i<20;i++)
		u[i]=-0.2;

/**
 Boundary condition
*/
	u[0] = 1.;
	u[nx-1]=1.;
/**
 begin the time loop
*/
	FILE *fic = fopen("data.txt", "w");
	fprintf(fic, "%s %s %s\n", "#x", "#u", "#t");
	
	while(t<=tend)
	{
		for(int i=0;i<nx;i++)
			fprintf(fic, "%g %g %g\n", x[i], u[i], t);
		fflush(fic);
		memcpy(U,u,sizeof(double)*nx);
		for(int i=1;i<nx;i++)
			u[i] = U[i] - (U[i]*dt/dx)* (U[i] - U[i-1]);
		fprintf(fic,"\n\n");
		t+=dt;
	}
	free(U); 
	free(x);
	free(u);
	fclose(fic);
	return 0;
}
