#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

struct Parametre
{
	int Longueur;
	int Nbrepoints;
	int tend;
};

int main(void)
{
	struct Parametre p = {2,81,2};

	double dx= (double) p.Longueur/( (double) p.Nbrepoints-1);
	double dt=dx/2.;
	double*x=NULL,*U=NULL, *u=NULL;	
/**
 dynamic allocation 
*/
	x= calloc(p.Nbrepoints,sizeof(double));
	U= calloc(p.Nbrepoints,sizeof(double));
	u= calloc(p.Nbrepoints,sizeof(double));
  
/**
 Boundary condition
*/
	u[0] = 0.;
	u[p.Nbrepoints-1]=0.;
	
/**
initial elevation: a "bump", position 
*/
	double t=0.;
	for(int i=0; i<p.Nbrepoints;i++)
	{
		x[i+1] = x[i] + dx;
		u[i] = 1.;
	}
	for(int i =10; i<20;i++)
		u[i]=2.;

/**
 Boundary condition
*/
	u[0] = 1.;
	u[p.Nbrepoints-1]=1.;
/**
 begin the time loop
*/
	FILE *fic = fopen("data.txt", "w");
	fprintf(fic, "%s %s %s\n", "#x", "#u", "#t");
	
	while(t<=p.tend)
	{
		for(int i=0;i<p.Nbrepoints;i++)
			fprintf(fic, "%g %g %g\n", x[i], u[i], t);
		fflush(fic);
		memcpy(U,u,sizeof(double)*p.Nbrepoints);
		for(int i=1;i<p.Nbrepoints;i++)
			u[i] = U[i] - (U[i]*dt/dx)* (U[i] - U[i-1]);
		fprintf(fic,"\n\n");
		t+=dt;
	}

	free(U); 
	free(x);
	free(u);
	fclose(fic);
	return 0;
}
