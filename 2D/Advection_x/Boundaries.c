#include <stdlib.h>
#include "Boundaries.h"
void top_dirichlet(double **mat, size_t n){
  for(int i=0; i<n; i++)
    mat[i][n-1]=0.;
}

void bottom_dirichlet(double **mat, size_t n){
  for(int i=0; i<n; i++)
    mat[i][0]=0.;
}

void left_dirichlet(double **mat, size_t n){
  for(int i=0; i<n; i++)
    mat[0][i]=0.;
}

void right_dirichlet(double **mat, size_t n){
  for(int i=0; i<n; i++)
    mat[n-1][i]=0.;
}

void left_neumann(double **mat, size_t n){
  for(int i=0; i<n; i++)
    mat[0][i]=mat[1][i];
}
