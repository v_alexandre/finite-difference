Hello,

This code simulate 2D advection and diffusion  using explicite finite difference. 

```math
\frac{\partial{U}}{\partial{t}} + U \frac{\partial{U}}{\partial{x}}=1/Re(\frac{\partial^{2}U}{\partial x^{2}} + \frac{\partial^{2} U}{\partial y^{2}})
```
First order derivative in time: 
```math
\frac{\partial{U}}{\partial{t}} = \frac{U^{t+1}_{i} - U^{t}_{i}}{\Delta t} 
```
Upwind first order :
```math
\frac{\partial{U}}{\partial{x}} = \frac{U_{i}^{t} - U_{i-1}^{t}}{\Delta x}
```
Second order second derivative: 
```math
\frac{\partial^{2}U}{\partial x^{2}} = \frac{U^{t}_{i+1} + U^{t}_{i-1} - 2U^{t}_{i}}{\Delta x^{2}}
```

which gives using euler explicite method :

```math
U^{t+1}_{i} = U^{t}_{i}(1+\frac{\Delta t}{\Delta x}(U^{t}_{i-1} - U^{t}_{i})) + \frac{\Delta t}{Re\Delta x^{2}}(U^{t}_{i+1} + U^{t}_{i-1} -2U^{t}_{i} + U^{t}_{j+1} + U^{t}_{j-1} -2U^{t}_{j})
```
with U>0

How to compile : gcc *.c -o prog -lm

How to run : ./prog

How to plot : gnuplot -p plot.gnu


![Gnuplot ouput](2D/Advection_x/pois.png)
