#include <stdlib.h>
#include "arr_copy.h"
void arr_copy(double **dest, double **src, size_t n){
  for(int i=0; i<n; i++){
    for(int j=0; j<n; j++){
      dest[i][j]=src[i][j];
    }
  }
}
