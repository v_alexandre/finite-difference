set terminal pngcairo size 1200,800
set output "pois.png"
load 'moreland.pal'
set size ratio 1
set pm3d map interpolate 1,1
#set contour
set dgrid3d 81 81 81
set key font ",15"
set tics font ",15"
set title "u.x"
set xlabel "x" font ",15" offset 0, -0.5
set ylabel "y" font ",15"  offset -0.5, 0
splot 'data.txt' u 1:2:($4==10.?$3:1/0)