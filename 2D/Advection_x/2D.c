#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "alloc2D.h"
#include "free2D.h"
#include "Boundaries.h"
#include "arr_copy.h"
struct Parametre
{
  double Longueur;
  size_t Nbrepoints;
  double tend;
  double Re;
};

int main(void)
{
  struct Parametre p = {2.,81,10.,2000.};
  /**
     Main parameter
  */
  double dx= p.Longueur/(p.Nbrepoints-1);
  double dt=dx/2.;	
  /**
     dynamic allocation 
  */
  double *x= calloc(p.Nbrepoints,sizeof(double));
  double **U= arr_alloc(p.Nbrepoints,p.Nbrepoints);
  double **u= arr_alloc(p.Nbrepoints,p.Nbrepoints);
  /**
     grid
  */
  for(int i=0; i<p.Nbrepoints; i++)
    x[i+1] = x[i] + dx;	
  
  /**
     Boundary condition
  */
  right_dirichlet(u,p.Nbrepoints);
  left_neumann(u,p.Nbrepoints);
  top_dirichlet(u,p.Nbrepoints);
  bottom_dirichlet(u,p.Nbrepoints);
  /**
     Initial condition
  */
  for(int i=0; i<p.Nbrepoints; i++)
    u[0][i]=(4*i*dx/p.Longueur)*(1-i*dx/p.Longueur);
  /**
     begin the time loop
  */
  FILE *fic = fopen("data.txt", "w");
  fprintf(fic, "%s %s %s %s\n", "#x", "#y", "#u", "#t");
  double t=0;
  while(t<=p.tend)
    {
      for(int i=0;i<p.Nbrepoints;i++){
	for(int j=0; j<p.Nbrepoints; j++){
	  fprintf(fic, "%g %g %g %g\n", x[i], j*dx, u[i][j], t);
	}
      }
      fflush(fic);
      arr_copy(U,u,p.Nbrepoints);
      for(int i=1;i<p.Nbrepoints-1;i++){
	for(int j=1; j<p.Nbrepoints-1;j++){
	  u[i][j] = U[i][j] *(1 + (dt/dx)*(U[i-1][j] - U[i][j])) + (dt/p.Re*pow(dx,2))*(U[i+1][j] + U[i-1][j] -2*U[i][j] + U[i][j+1] + U[i][j-1] -2*U[i][j]);
	}
      }
      fprintf(fic,"\n\n");
      t+=dt;
    }
  /**
     free memories
  */
  arr_free(U, p.Nbrepoints, p.Nbrepoints);
  arr_free(u, p.Nbrepoints, p.Nbrepoints);
  free(x);
  return 0;
}
