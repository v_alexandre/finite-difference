#include <stdlib.h>
#include <assert.h>
#include "alloc2D.h"
double** arr_alloc (size_t x, size_t y)
{
  double** pp = calloc(x,sizeof(*pp));
  assert(pp != NULL);
  for(size_t i=0; i<x; i++)
  {
    pp[i] = calloc(y,sizeof(**pp));
    assert(pp[i] != NULL);
  }

  return pp;
}
