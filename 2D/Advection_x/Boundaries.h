void top_dirichlet(double **mat, size_t n);
void bottom_dirichlet(double **mat, size_t n);
void left_dirichlet(double **mat, size_t n);
void right_dirichlet(double **mat, size_t n);
void left_neumann(double **mat, size_t n);
